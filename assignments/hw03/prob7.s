    .set noreorder
    .data

    .text
    .globl main
    .ent main
print:
	#void print(int a)
	#{ 
	#       // should be implemented with syscall 20
	ori $v0, $0, 20  # print
	syscall          # call
	jr $ra     		 # jump to ra
	nop
	#} 
sum3:
	#int sum3(int a, int b, int c)
	#{ 
	#       return a+b+c;
	add $t0, $a0, $a1  # add t0 a0 a1
	add $v0, $t0, $a2  # add v0 t0 a2
	jr $ra             # jump to ra
	nop
	#} 
polynomial:
	#int polynomial(int a, int b, int c, int d, int e)
	#{ 
	#       int x;
	#       int y;
	#       int z;
	#       x = a << b;
	addi $sp, $sp, -8  # mvoe stack pointer
	sw $s0, 0($sp)     # store s0 on stack
	sw $ra, 4($sp)     # store ra on stack 
	sllv $s0, $a0, $a1
	#       y = c << d;
	sllv $s1, $a2, $a3
	#       z = sum3(x, y, e);
	lw $t0, 8($sp)
	add $a0, $s0, $0
	add $a1, $s1, $0
	add $a2, $t0, $0
	jal sum3
	nop

	add $s2, $0, $v0





	#       print(x);
	add $a0, $0, $s0
	jal print
	nop
	#       print(y);
	add $a0, $0, $s1
	jal print
	nop
	#       print(z);
	add $a0, $0, $s2
	jal print
	nop
	#       return z0;

	add $v0, $s2, $0
    lw $ra, 4($sp)
	lw $s0, 0($sp)
	add $sp, $sp, 8
	
	jr $ra
	nop
	#} 
main:
	#int    main()
	#{ 
	#       int a = 2;
	addi $s0, $0, 2
	#       int f = polynomial(a, 3, 4, 5, 6);
	addi $sp, $sp, -16
	sw $ra, 12($sp)
	sw $s0, 8($sp)
	sw $s1, 4($sp)
	
	add $a0, $0, $s0
	addi $a1, $0, 3
	addi $a2, $0, 4
	addi $a3, $0, 5
	addi $t0, $0, 6
	sw $t0, 0($sp)
	jal polynomial
	nop
	add $s1, $v0, $0
	#       print(a);
	add $a0, $s0, $0

	jal print
	nop
	#       print(f);
	add $a0, $s1, $0
	jal print
	nop

	lw $ra, 12($sp)
	lw $s0, 8($sp)
	lw $s1, 4($sp)
	addi $sp, $sp, 16
	jr $ra 
	nop
	#} 
        ori $v0, $0, 10     # exit
        syscall
    .end main
