    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
	# typedef struct record {
	#       int field0;
	#       int field1;
	#} record;
# there is a struct, of size of two ints

	#int main()
	#{ 
	#       record *r = (record *) MALLOC(sizeof(record));
	addi $a0, $0, 8    # sizeof(record)
	ori $v0, $0, 9     # malloc
	syscall            # makes call
	#       r->field0 = 100;
	addi $t0, $0, 100  # store 100
	sw $t0, 0($v0)     # put 100 into memory
	
	#       r->field1 = -1;  
	addi $t0, $0, -1   # store -1
	sw $t0, 4($v0)     # put -1 in memory
	#       EXIT;
	#} 


        ori $v0, $0, 10     # exit
        syscall
    .end main
