    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:



#int main()
#{
# int score = 84;
    addi $s0, $0, 84 # s0 = 84
# int grade;  # s1
# if (score >= 90)  
    #sgt t0 s0 90 
    slti $t0, $s0, 90  # t0 = s0 < 90
    #beq 
    bne $t0, $0, sequel # if not less than, go to seuqul
    nop


# grade = 4;
    addi $s1, $0, 4 # s1 = 4
    j print
sequel:
# else if (score >= 80)
    slti $t0, $s0, 80  # t0 = s0 < 80
    bne $t0, $0, sequelone   # if not = 0, so to sequqel 1
	nop
# grade = 3;
    addi $s1, $0, 3  #  s1 = 3
    j print          # print
sequelone:
# else if (score >= 70)
    slti $t0, $s0, 70   # t0 = s0 < 70
    bne $t0, $0, sequeltwo   # if so, go to sequeltwo
	nop
# grade = 2;
    addi $s1, $0, 2    # s1 = 2
    j print
# else
sequeltwo:
# grade = 0;
    addi $s1, $0, 0    # s1 = 0
    j print
# PRINT_HEX_DEC(grade);
print:
    add $a0, $0, $s1   # a0 = s1
    ori $v0, $0, 20    # v= 0 0 | 20
    syscall
# EXIT;
        ori $v0, $0, 10     # exit
        syscall
.end main
#}

