    .set noreorder
    .data
#char A[] = { 1, 25, 7, 9, -1 };    
	 A:  .byte   1, 25, 7, 9, -1

    .text
    .globl main
    .ent main
#int main()
#{ 
main:

#       int i;
# s0 = i
#       int current;
# s1 = current
#       int max;
# s2 = max
#       i = 0;
# add i 0 
	add $s0, $0, $0

#       current = A[0];
# load s1 A[0]
	lui $t0, %hi(A)  # load first half of address of A
	ori $t0, $t0, %lo(A)  # load second half of address of a
	lb $s1, 0($t0)    # load in value from A[0]


#       max = 0;

	addi $s2, $0, 0

#       while (current > 0) {
start:
	slt $t1, $0, $s1
	beq $t1, $0, out
	nop


#if (current > max)
	slt $t1, $s2, $s1
	beq $t1, $0, skip
	nop
#   max = current;
	add $s2, $0, $s1
skip: 
#i = i + 1;
	addi $s0, $s0, 1
#current = A[i];
	add $t0, $t0, $s0
	lb $s1, 0($t0)
	
	j start

#       } 
out:
#       PRINT_HEX_DEC(max);
    add $a0, $0, $s2
    ori $v0, $0, 20
    syscall
#       EXIT;
#} 
        ori $v0, $0, 10     # exit
        syscall
    .end main
