    .set noreorder
    .data
	A: .space 32

    .text
    .globl main
    .ent main
main:

	#Problem 3
	# (10 points)
	#int A[8]
	lui $t0, %hi(A)  # store value of address
	ori $t0, $t0, %lo(A)  # store second half of address
	#int main()
	#{ 
	#       int i;
		# s0 = i
	#       A[0] = 0;
	addi $t1, $0, 0  # A[0] = 0
	sw $t1, 0($t0)   # A[0] = 0
	#       A[1] = 1;
	addi $t1, $0, 1  #A[1] = 1
	sw $t1, 4($t0)   #A[1] = 1
	#   for (i = 2;  i < 8;  i++) {
	addi $s0, $0, 2  # i = 2

looper:
	slti $t2, $s0, 8  # i < 8
	beq $0, $t2, exit # i < 8
	nop
	#   A[i] = A[i-1] + A[i-2];   
	
	sll $t3, $s0, 2 # i*4 (number of bits)
	# find where i is in reference to A[0]
	add $t4, $t0, $t3  # $t4 = A[i]  
	
	lw $t1, -4($t4) # t1 = value at t5
	lw $t2, -8($t4) # t2 = value at t6

	add $t3, $t1, $t2 # A[i-1] + A[i-2]

	sw $t3, 0($t4) # write value of addition to memory A[i]
	
		
	#   PRINT_HEX_DEC(A[i]);

    add $a0, $0, $t3 # print
    ori $v0, $0, 20  # print 
    syscall          # call that prints

	addi $s0, $s0, 1  # i++
	j looper    	 # goes back to top of loop
	nop
	#       } 

exit:

	#       EXIT;
	#} 
        ori $v0, $0, 10     # exit
        syscall             # call that exits
    .end main
