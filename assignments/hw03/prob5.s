    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
	#typedef struct elt {
	#       int value;
	#       struct elt *next;
	#} elt;
	#int main()
	#{ 
	#       elt *head;
	#       elt *newelt;
	#       newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8    # sizeof(record)
	ori $v0, $0, 9     # malloc
	syscall            # makes call
	#       newelt->value = 1;
	addi $t0, $0, 1  # store 1
	sw $t0, 0($v0)     # put 1 into memory
	#       newelt->next = 0;
	addi $t0, $0, 0  # store 0
	sw $t0, 4($v0)     # put 0 into memory
	#       head = newelt;
	add $s0, $0, $v0   # add 0 , #0  head = s0
	#       newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8    # sizeof(record)
	ori $v0, $0, 9     # malloc
	syscall            # makes call
	#       newelt->value = 2;
	addi $t0, $0, 2    # t0 = 2
	sw $t0, 0($v0)     # v0 = t0
	#       newelt->next = head;
	add $t0, $0, $s0  # t0 = head
	sw $t0, 4($v0)     # v0 + 4 = t0
	#       head = newelt;
	add $s0, $0, $v0
	#       PRINT_HEX_DEC(head   ->value);
	lw $t0, 0($s0)   # value into t0
    add $a0, $0, $t0 # print
    ori $v0, $0, 20  # print 
    syscall          # call that prints
	#       PRINT_HEX_DEC(head   ->next->value);
	lw $t0, 4($s0)   # value into t0
	lw $t1, 0($t0)   # dereference the address at t0
    add $a0, $0, $t1 # print
    ori $v0, $0, 20  # print 
    syscall          # call that prints
	#       EXIT;
	#} 
        ori $v0, $0, 10     # exit
        syscall
    .end main
