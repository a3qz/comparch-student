    .set noreorder
    .data
	A: .space 32
    .text
    .globl main
    .ent main

print:
	#void print(int a)
	#{ 
	ori $v0, $0, 20  # print
	syscall          # call
	jr $ra
	nop
	#       // should be implemented with syscall 20
	#} 
	#int main()
main:
	#{ 
	#       int A[8];
	#       int i;
	lui $t0, %hi(A)  # store value of first half
	ori $t0, $t0, %lo(A)  # store second half
	#       A[0] = 0;
	addi $t1, $0, 0 	#t1 = 0
	sw $t1, 0($t0) 		#A[0] = 0
	#       A[1] = 1;
	addi $t1, $0, 1     #t1 = 1
	sw $t1, 4($t0)      #A[1] = 0
	#       for (i = 2;  i < 8;  i++) {
	addi $s0, $0, 2     #i = 2

looper:
	slti $t2, $s0, 8   # i < 8
	beq $0, $t2, exit  # if resutl is 0
	nop

	#A[i] = A[i-1] + A[i-2];   

	sll $t3, $s0, 2  # i * 4 (number of bits)
	add $t4, $t0, $t3    # t4 = A[i]

	lw $t1, -4($t4)  # t1 = value at A[i-1]
	lw $t2, -8($t4)  # t1 = value at A[i-2]

	add $t3, $t1, $t2  # t3 = A[i-1] + A[i-2]

	sw $t3, 0($t4)    # write t3 to memory position at t4
	#print(A[i]);
	add $a0, $0, $t3  # store value to print a0 = t3 (t4 is A[0])
	jal print         # jump to print function
	nop               # nop
	addi $s0, $s0, 1  # s0 = s0 +1 ( i++)
	j looper          # jump to top of lopp
	nop               # nop
	#       } 
	#} 
exit:
        ori $v0, $0, 10     # exit
        syscall
    .end main
